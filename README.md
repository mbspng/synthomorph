# Synthomorph

The *synthomorph* module generates morphologically constrained instantiatons of phrases
based on user-defined templates and a database of morphologically annotated word forms.

# Usage Example

```python
>>> from synthomorph import generate
>>>
>>> template = [
...     "schalte",
...     {"morphology": "pos:ART Acc gen:$y num:$x deriv_fl:$f"},
...     {"morphology": "ADJA Acc gen:$y lemma:blau,rot,gelb num:$x deriv_fl:$f"},
...     {"morphology": "NN Acc gen:$y lemma:lampe,licht,beleuchtung, num:$x"},
...     "aus"
... ]
>>>
>>> for p in generate([template]):
...     print(" ".join(p))
...
schalte die rote beleuchtung aus
schalte die rote lampe aus
schalte die blaue beleuchtung aus
schalte die blaue lampe aus
schalte die gelbe beleuchtung aus
schalte die gelbe lampe aus
schalte die roten lampen aus
schalte die roten lichter aus
schalte die roten beleuchtungen aus
schalte die blauen lampen aus
schalte die blauen lichter aus
schalte die blauen beleuchtungen aus
schalte die gelben lampen aus
schalte die gelben lichter aus
schalte die gelben beleuchtungen aus
schalte das rote licht aus
schalte das blaue licht aus
schalte das gelbe licht aus
schalte ein rotes licht aus
schalte ein blaues licht aus
schalte ein gelbes licht aus
schalte eine rote beleuchtung aus
schalte eine rote lampe aus
schalte eine blaue beleuchtung aus
schalte eine blaue lampe aus
schalte eine gelbe beleuchtung aus
schalte eine gelbe lampe aus
```

# Installation

Install the module as follows:

    git clone git@gitlab.com:mbspng/synthomorph.git
    cd synthomorph
    pip install -e .


Furthermore, you must provide a morphological database. You can produce one by running

    smor-lemmatizer data/fasttext_vocab.txt > data/morph-db.txt

*smor-lemmatizer* can be obtained as part of the software *SMOR*: `https://www.cis.lmu.de/~schmid/tools/SMOR/`
Note that the license of this here reporistory (`git@gitlab.com:mbspng/synthomorph.git`) does not apply to the
software *SMOR*.
