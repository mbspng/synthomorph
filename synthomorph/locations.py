from os import path

ROOT_DIR = path.dirname((path.dirname(path.abspath(__file__))))
DATA_DIR = path.join(ROOT_DIR, "data")
LABELS_FILE = path.join(DATA_DIR, "labels.csv")
TAGSET_FILE = path.join(DATA_DIR, "tagset.csv")
MORPH_DB_FILE_PRQ = path.join(DATA_DIR, "morph-db.parquet")
MORPH_DB_FILE_TXT = path.join(DATA_DIR, "morph-db.txt")
FASTTEXT_FILE = path.join(DATA_DIR, "fasttext_vocab.txt")
