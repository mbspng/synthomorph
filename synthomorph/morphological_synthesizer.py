"""
MIT License

Copyright (c) 2021 Matthias Bisping

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


# TODO:
#   - Represent feature map as ordinary dicts?
#   - Add a config for mapping the hardcoded constants like "Wk" and "NoGend" to values from a different schema
#   - ... or swap out SMOR (not commercially usable without paid license) schema for DeMorphy (MIT license) schema
#   - Improve algorithm complexity


import json
import os
import re
import sys
from collections import defaultdict
from itertools import product
from itertools import starmap
from operator import itemgetter
from typing import Iterable
from typing import List, Dict, Union

import numpy as np
import pandas as pd
from frozendict import frozendict
from more_itertools import flatten
from tqdm import tqdm

from synthomorph.locations import LABELS_FILE, TAGSET_FILE, MORPH_DB_FILE_PRQ, MORPH_DB_FILE_TXT, FASTTEXT_FILE
from synthomorph.utils import starfilter


class MorphologicalSynthesizer:
    def __init__(self, tag_cat_df: pd.DataFrame, wml_database: Union[List, pd.DataFrame], store_db: bool = True):
        """Given a morphological database that maps tokens to their morphological attributes, transforms templates of
        the form (string|dict)^n where `dict` has the form {"morphology": <feature map>} where `feature` map can look
        like "pos:ART Acc gen:$y num:$x", into instantiations of the templates that satisfy the annotated morphological
        constraints.

        Args:
            tag_cat_df: DataFrame with labels and their categories
            wml_database: Either this is a list of word-morphology-lemma tokens to build a databse from, in which case
                the tokens look like this:

                    ihrer	PPER.3.Sg.Fem.Gen sie
                    ihrer	PPER.3.Pl.NoGend.Gen sie
                    ihrer	PPOSAT.Fem.Gen.Sg.St ihre
                    ...

                Or it is a prepared database in .parquet format.
            store_db: if a list of tokens rather than a prepared database was passed to wml_database, the list of tokens
                will be parsed into a pandas DataFrame. For faster loading in future instantiations of
                MorphologicalSynthesizer the dataframe can be stored in .parquet format.
        """
        self.label2cat = dict(zip(tag_cat_df.label.values, tag_cat_df.category.values))
        self.categories = set(self.label2cat.values())
        self.data_template = {**{"word": [], "lemma": []}, **{c: [] for c in self.categories}}
        self.word_morphology_lemma_pattern = re.compile(r"^(\w+)\s+(:?(:?\w+\.)*(:?\w+))\s+(\w+)$")
        if isinstance(wml_database, pd.DataFrame):
            self.database = wml_database
        elif isinstance(wml_database, list):
            self.database = self.__parse_wml_tokens(wml_database)
            if store_db:
                print(f"Wrote database to {MORPH_DB_FILE_PRQ}", file=sys.stderr)
                self.database.to_parquet(MORPH_DB_FILE_PRQ, engine="pyarrow")
        else:
            raise ValueError("Invalid format for wml_database")

        self.feature_value_delimiter = ":"
        self.mfield_key = "morphology"

    def __add_features_to_table(self, features, table):
        table["word"].append(features["word"].lower())
        table["lemma"].append(features["lemma"].lower())

        for c in self.categories:
            table[c].append(np.nan)

        for ftr in features["morphology"]:
            c = self.label2cat[ftr]
            table[c][-1] = ftr

    def __parse_wml_tokens(self, wml_tokens: List[str]):
        """Parses word-morphology-lemma tokens into a dataframe. Tokens look like 'ihrer PPER.3.Sg.Fem.Gen sie'"""

        def process_wml_token_and_add_to_table(wml_token, table):
            def parse_token_to_features(string):
                try:
                    word, morpho_features, *_, lemma = self.word_morphology_lemma_pattern.match(string).groups()
                except AttributeError:
                    raise ValueError(
                        f"token {string} does not match the pattern `<word> <morphological features> <lemma>`"
                    )
                morpho_features = [f if not f == "$" else f + "." for f in morpho_features.split(".") if f]
                return {"word": word, "lemma": lemma, "morphology": morpho_features}

            try:
                features = parse_token_to_features(wml_token)
            except ValueError as e:
                raise e
            self.__add_features_to_table(features, table)

        data = self.data_template.copy()
        for wml_token in tqdm(wml_tokens, desc="parsing database"):
            try:
                process_wml_token_and_add_to_table(wml_token, data)
            except ValueError:
                continue  # skip tokens that are not formatted correctly (e.g. missing morph. features)
        return pd.DataFrame(data).drop_duplicates()

    def __filter_specification_for_variables(self, spec: frozendict):
        return self.__filter_specification(spec, True)

    def __filter_specification_for_constants(self, spec: frozendict):
        return self.__filter_specification(spec, False)

    @staticmethod
    def __filter_specification(spec: frozendict, filter_for_variables: bool):
        """Filters a token specificaton for features with variables as values or constants as values."""

        def is_variable(v):
            return v[0] == "$"

        def value_is_variable(_, value):
            return is_variable(value)

        def value_is_constant(_, value):
            return not is_variable(value)

        func = (value_is_constant, value_is_variable)[filter_for_variables]

        return dict(starfilter(func, spec.items()))

    def generate_alternate_instantiations(self, phrase: str):
        def process_morphology(token_id, morpho_spec: str):
            """Parses morphological specifications of a token and appends entry to `spec_sequence`"""

            def make_feature_value_maps(instances, feature_value_pairs):
                def dissolve_dataframe_into_feature_value_maps(instances):
                    # create (dot-indexable) dictionaries from the rows of `instances`
                    return [frozendict(d) for d in instances.to_dict("records")]

                def rename_category_columns_as_variables(instances, feature_value_pairs):
                    # rename the columns to match the variables: ('cas', '$x') -> '$x'
                    return instances.rename(columns=dict(feature_value_pairs))

                def map_variables_to_token_indices(variables) -> None:
                    """Establishes which tokens share a variable."""
                    for variable in variables:
                        variables_to_token_indices[variable].append(token_id)

                features, variables = zip(*feature_value_pairs)

                map_variables_to_token_indices(variables)
                # reduce morphological information to relevant features
                instances = instances[["word"] + list(features)]
                instances = rename_category_columns_as_variables(instances, feature_value_pairs)
                return dissolve_dataframe_into_feature_value_maps(instances)

            def parse_morphological_specification(spec: str):
                """Parses the string representation of a  morphological specification.

                Args:
                    spec: string of morphological specifications for a token (e.g. 'ART cas:$x Sg Masc').

                Returns:
                    Mapping from feature names to values (e.g. {'pos': 'ART', 'cas': '$x', 'num': 'Sg', 'gen': 'Masc'}).
                """
                spec = spec.split(" ")

                def split_spec_into_feature_and_variable(spec):
                    return spec.split(self.feature_value_delimiter)

                def look_up_feature_for_value_and_make_feature_value_pair(spec):
                    return self.label2cat[spec], spec

                def parse_spec(spec):
                    return (
                        split_spec_into_feature_and_variable
                        if self.feature_value_delimiter in spec
                        else look_up_feature_for_value_and_make_feature_value_pair
                    )(spec)

                return frozendict(map(parse_spec, spec))

            def find_instances_of_morphological_specification(spec: frozendict):
                """Filters the database for rows where the columns specified in `spec.keys()` contain the values
                specified in `spec.values()`

                Args:
                    spec: Specification to filter for.

                Returns:
                    Filtered database.
                """

                def normalize(values):
                    return "[" + ", ".join(f"'{v}'" for v in values.split(",")) + "]"

                spec = self.__filter_specification_for_constants(spec)

                query = " & ".join(f"{k} in {normalize(v)}" for k, v in spec.items())
                return self.database.query(query)

            spec = parse_morphological_specification(morpho_spec)
            instances = find_instances_of_morphological_specification(spec)
            spec = self.__filter_specification_for_variables(spec)

            return make_feature_value_maps(instances, spec.items())

        def is_congruent(spec_sequence):
            def tokens_match_in_feature(feature, specs):
                def invert_complement_values_for_definiteness(values_for_feature_across_tokens):
                    def flip(val):
                        assert val in ("St", "Wk")
                        return "Wk" if val == "St" else "St"

                    # TODO: refactor this mess
                    if {*values_for_feature_across_tokens}.intersection({"Wk", "St"}):
                        values_for_feature_across_tokens = [values_for_feature_across_tokens[0]] + [
                            flip(v) for v in values_for_feature_across_tokens[1:]
                        ]
                    return values_for_feature_across_tokens

                # TODO: refactor this mess
                values_for_feature_across_tokens = [*map(itemgetter(feature), specs)]
                values_for_feature_across_tokens = invert_complement_values_for_definiteness(
                    values_for_feature_across_tokens
                )
                values_for_feature_across_tokens = set(values_for_feature_across_tokens)

                n = len(values_for_feature_across_tokens)

                def gather_values(spec):
                    return {*flatten([v] for v in spec.values())}

                # only one value means congruence
                if n == 1:
                    return True
                elif n == 2:
                    # special plural rule: deinen zimmern (neut), deinen gaerten (mask), deinen katzen (fem) etc.
                    # "deinen" etc. have no gender anymore according to the DAG
                    if values_for_feature_across_tokens.intersection({"NoGend", None}) and all(
                        "Pl" in gather_values(spec) for spec in specs
                    ):
                        return True
                # more than one value (exlcuding None) means no congruence
                else:
                    return False

            return all(
                tokens_match_in_feature(feature_variable, itemgetter(*token_indices)(spec_sequence))
                for feature_variable, token_indices in variables_to_token_indices.items()
            )

        def token_is_morphologically_annotated(token: Dict):
            return self.mfield_key in token

        def assemble_instantiated_sequence(sequence):
            for i, t in enumerate(sequence):
                tokens[indices[i]] = t
            yield tokens

        def filter_duplicates(sequeces):
            seen = set()
            for sequence in sequeces:
                h = hash(sequence)
                if h not in seen:
                    seen.add(h)
                    yield sequence

        variables_to_token_indices = defaultdict(list)

        tokens = json.loads(phrase) if isinstance(phrase, str) else phrase
        indices, mtokens = zip(*((i, t) for i, t in enumerate(tokens) if token_is_morphologically_annotated(t)))
        specs = map(itemgetter(self.mfield_key), mtokens)
        spec_alternatives_per_token = starmap(process_morphology, enumerate(specs))

        unique_sequences = filter_duplicates(product(*spec_alternatives_per_token))
        congruent_spec_sequences = filter(is_congruent, unique_sequences)
        congruent_tokens_sequences = map(lambda s: map(itemgetter("word"), s), congruent_spec_sequences)

        return flatten(map(assemble_instantiated_sequence, congruent_tokens_sequences))


def load_morph_db(morph_db_file=None):
    def load_parquet_db(morph_db_file):
        return pd.read_parquet(morph_db_file, engine="pyarrow")

    def load_txt_db(morph_db_file):
        with open(morph_db_file) as f:
            return [l.strip() for l in f]

    if morph_db_file is None:
        if os.path.isfile(MORPH_DB_FILE_PRQ):
            return load_parquet_db(MORPH_DB_FILE_PRQ)

        elif os.path.isfile(MORPH_DB_FILE_TXT):
            return load_txt_db(MORPH_DB_FILE_TXT)
        else:

            error_message = f"""
            You must provide a morphological database. You can produce one by running
            
                > smor-lemmatizer {FASTTEXT_FILE} > {MORPH_DB_FILE_TXT}.
                
            'smor-lemmatizer' can be obtained as part of the software 'SMOR': https://www.cis.lmu.de/~schmid/tools/SMOR/
            Note that the license of this reporistory (git@gitlab.com:mbspng/morpho_synth.git) does not apply to the
            software 'SMOR'.
            """
            raise ValueError(error_message)
    else:
        if os.path.splitext(morph_db_file)[1] == ".txt":
            return load_txt_db(morph_db_file)
        if os.path.splitext(morph_db_file)[1] == ".parquet":
            return load_parquet_db(morph_db_file)
        else:
            raise ValueError(f"'{morph_db_file}' has unknown file format. Use `.txt` or `.parquet`")


def build_tag_cat_df(labels_file=LABELS_FILE, tag_set_file=TAGSET_FILE):
    df1 = pd.read_csv(labels_file, sep="\t")
    df2 = pd.read_csv(tag_set_file, sep=";")
    df2 = pd.DataFrame({"label": df2.tag.values, "category": ["pos"] * len(df2)})
    df = pd.concat([df1, df2])
    return df


def generate(templates: Iterable):
    """Generates instantiations for an iterable of templates.

    Args:
        templates: iterable of templates. Templates can look like this:
            ["schalte",
            {"morphology": "pos:ART Acc gen:$y num:$x"},
            {"morphology": "NN Acc gen:$y lemma:lampe num:$x"},
            "aus"]

    Returns:
        instantiations of the templates
    """
    df = build_tag_cat_df()
    lexicon = load_morph_db()
    morphological_synthesizer = MorphologicalSynthesizer(df, lexicon)

    return flatten(map(morphological_synthesizer.generate_alternate_instantiations, templates))
