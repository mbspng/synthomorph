"""
MIT License

Copyright (c) 2021 Matthias Bisping

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import argparse
from synthomorph import generate


def parse_template_file(phrase_file_path):
    with open(phrase_file_path) as f:
        return [l.strip() for l in f if not l[0] == "#" and l.split()]


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("phrase_file_path")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()

    templates = parse_template_file(args.phrase_file_path)

    for p in generate(templates):
        print(" ".join(p))